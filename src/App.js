import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {connect} from 'react-redux';
import HeaderContainer from "./containers/HeaderContainer";
import BalancesContainer from "./containers/BalancesContainer";
import actionCreators from "./actions/actionCreators";
import {bindActionCreators} from 'redux';

class App extends Component {

    componentDidMount() {
        this.props.onLoadData();
    }

    render() {
        return (<div>
            <HeaderContainer />
            <BalancesContainer />
        </div>);
    }
}

const mapDispatchToProps = (dispatch) => ({
    onLoadData: bindActionCreators(actionCreators.loadAccounts, dispatch)
});

export default connect(null, mapDispatchToProps)(App);
