export default [
    {
        "address": "0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2",
        "symbol": "MKR",
        "decimal": 18
    },
    {
        "address": "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
        "symbol": "ETH",
        "decimal": 18
    },
    {
        "address": "0xbeb9ef514a379b997e0798fdcc901ee474b6d9a1",
        "symbol": "MLN",
        "decimal": 18
    },
    {
        "address": "0x89d24A6b4CcB1B6fAA2625fE562bDD9a23260359",
        "symbol": "DAI",
        "decimal": 18
    }
];