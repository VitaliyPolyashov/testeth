import React, {Component} from 'react';
import SendTokenPanel from './SendTokenPanel';

class TokenView extends Component {
    componentDidMount() {
        //this.props.onEquivalentsLoad();
    }

    render() {
        return (
            <table className="table w-100">
                <thead>
                <tr>
                    <th scope="col-4">Name</th>
                    <th scope="col-4">Amount</th>
                    <th scope="col-4">Equivalent</th>
                    <th scope="col-4"></th>
                </tr>
                </thead>
                <tbody>
                {this.props.tokens.map(token => {
                    return [<tr key={token.symbol}>
                        <td>
                            {token.symbol}
                        </td>
                        <td>
                            {token.amount}
                        </td>
                        <td>
                            {token.isLoading &&
                            <div><img src="https://cdnjs.cloudflare.com/ajax/libs/galleriffic/2.0.1/css/loader.gif" /></div>}
                            {!token.isLoading &&
                            `$ ${token.equivalent}`
                            }
                        </td>
                        <td>
                            <button className="btn btn-secondary"
                                    onClick={this.props.onTransferring.bind(this, token.address)}
                            >Transfer
                            </button>
                        </td>
                    </tr>,
                        token.isTransferring ?
                            <tr><td colSpan={4}><SendTokenPanel
                                //amount={token.sendAmount}
                                maxAmount={token.amount}
                                sourceAddress={this.props.address}
                                tokenAddress={token.address}
                                onSend={this.props.onSend}
                            /></td></tr>
                            : null]
                })}
                </tbody>
            </table>);
    }
}

export default TokenView;
