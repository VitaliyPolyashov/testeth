import React, {Component} from 'react';

class SendTokenPanel extends Component {
    constructor(props) {
        super(props);
        this.state = { amount: 0, address: ''}
    }
    
    handleSend() {
        this.props.onSend(this.props.tokenAddress, this.props.sourceAddress.value, this.state.address, this.state.amount)
    }

    handleValueChange(data) {
        this.setState({amount: +data.target.value});
    }

    handleAddressChange(data) {
        this.setState({address: data.target.value});
    }
    
    render() {
        return (
            <div className="row"
            >
                <label className="col-1">Amount</label>
                <div className="col-4 ">
                    <input className="form-control"
                           type="number"
                           value={this.state.amount}
                           max={this.props.maxAmount}
                           min={0}
                    onChange={this.handleValueChange.bind(this)}
                    />
                </div>
                <label className="col-1">Address</label>
                <div className="col-4">
                    <input className="form-control" type="text"
                           value={this.state.address}
                    onChange={this.handleAddressChange.bind(this)}
                    />
                </div>
                <div className="col-sm-2">
                <button
                    className="btn btn-primary "
                    onClick={this.handleSend.bind(this)}    
                >Send
                </button>
                </div>
            </div>);
    }
}

export default SendTokenPanel;