import React, { Component } from 'react';
import Select from 'react-select';

class AccountSelect extends Component {
  render() {
    return (
        <Select
            className="w-100"
            placeholder="Select address"
            options={this.props.options}
            value={this.props.value}
            onChange={this.props.onChange}
        />);
  }
}

export default AccountSelect;