import actionTypes from '../actions/actionTypes';
import {handleActions} from 'redux-actions';

const initialState = {
    isLoading: false,
    address: '',
    addresses: [],
    tokens: []
};

export default handleActions({
    [actionTypes.ADDRESSES_LOAD_INIT]: (state, action) => ({
        ...initialState,
        isLoading: true
    }),
    [actionTypes.ADDRESSES_LOAD_COMPLETE]: (state, action) => ({
        ...state,
        addresses: action.payload.addresses.map(x => ({value: x, 
            label: [x.slice(0, 7), 
                x.slice(x.length - 10)].join('...')}))
    }),
    [actionTypes.ADDRESS_CHANGED]: (state, action) => ({
            ...state,
            address: action.payload,
            tokens: []
        }
    ),
    [actionTypes.TOKENS_LOAD_COMPLETE]: (state, action) => {
        let tokens = state.tokens;
        if (!action.payload.data.isZero() && !tokens.find(x => x.address === action.payload.token)) {
            tokens = [...tokens, {
                ...action.payload.token,
                amount: +(action.payload.data.toString()) / Math.pow(10, action.payload.token.decimal),
                //equivalent: 0
            }];
        }
        return {
            ...state,
            tokens: tokens
        };
    },
    [actionTypes.EQUIVALENT_LOAD_INIT]: (state, action) => {
        let tokens = state.tokens;
        let tokenIndex = tokens.findIndex(x => x.address === action.payload.address);
        let newTokens = [...tokens.slice(0, tokenIndex),
            {
                ...tokens[tokenIndex],
                isLoading: true,
                
            },
            ...tokens.slice(tokenIndex + 1)];
        return {
            ...state,
            tokens: newTokens
        };
    },
    [actionTypes.EQUIVALENT_LOAD_COMPLETE]: (state, action) => {
        let tokens = state.tokens;
        let tokenIndex = tokens.findIndex(x => x.address === action.payload.token.address);
        let newTokens = [...tokens.slice(0, tokenIndex),
            {
                ...tokens[tokenIndex],
                isLoading: false,
                equivalent: (action.payload.equivalent * tokens[tokenIndex].amount).toFixed(5)
            },
            ...tokens.slice(tokenIndex + 1)];
        return {
            ...state,
            tokens: newTokens
        };
    },

    [actionTypes.TRANSFERRING_MODE_SET]: (state, action) => {
        let tokens = state.tokens.map(x => ({
            ...x,
            isTransferring: false
        }));
        let tokenIndex = tokens.findIndex(x => x.address === action.payload);
        let newTokens = [...tokens.slice(0, tokenIndex),
            {
                ...tokens[tokenIndex],
                isTransferring: true
            },
            ...tokens.slice(tokenIndex + 1)];
        return {
            ...state,
            tokens: newTokens
        };
    }
}, initialState);
