import React, {Component} from 'react';
import {connect} from 'react-redux';
import AccountSelect from '../components/AccountSelect';
import actionCreators from '../actions/actionCreators';
import {bindActionCreators} from 'redux';

class HeaderContainer extends Component {

    handleAddressChange(data) {
        this.props.onAddressChanged(data);
    }

    render() {
        return (<nav className="navbar navbar-dark bg-dark">
            <div className="navbar-brand" href="#">Token Explorer</div>
            <div className="navbar-right w-25">
            <AccountSelect
                value={this.props.address}
                options={this.props.addresses}
                onChange={this.handleAddressChange.bind(this)}
            />
            </div>
        </nav>);
    }
}

const mapStateToProps = (state) => ({
    addresses: state.addresses,
    address: state.address
});

const mapDispatchToProps = (dispatch) => ({
    onAddressChanged: bindActionCreators(actionCreators.changeAddress, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HeaderContainer);
