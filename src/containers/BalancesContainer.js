import React, {Component} from 'react';
import {connect} from 'react-redux';
import actionCreators from '../actions/actionCreators';
import {bindActionCreators} from 'redux';
import TokenView from '../components/TokenView';
import {Pie} from 'react-chartjs-2';

class BalancesContainer extends Component {
    constructor(props) {
        super(props);
        
        this.state = {colors:[]};
    }

    componentDidMount() {
        this.setState({colors: [1,2,3,4,5].map(x => (this.getRandomColor()))});
    }

    getRandomColor() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    
    render() {
        return (<div className="row">
            <div className="col-8">
            {this.props.address &&
            <TokenView
                tokens={this.props.tokens}
                address={this.props.address}
                onEquivalentsLoad={this.props.onEquivalentsLoad}
                onTransferring={this.props.onTransferring}
                onSend={this.props.onSend} /> }
            </div>
            <div className="col-4">
            <Pie
                data={{datasets: [{ 
                    data: this.props.tokens
                        .map(x => x.equivalent),
                        backgroundColor: this.state.colors
                        
                }],//{this.props.tokens.map(x => ({data : [x.equivalent]})),
                labels: this.props.tokens.map(x => x.symbol),
                    }}
                width={500}
                height={500}/>
            </div>
        </div>);
    }
}

const mapStateToProps = (state) => ({
    address: state.address,
    tokens: state.tokens
});

const mapDispatchToProps = (dispatch) => ({
    //onEquivalentsLoad: bindActionCreators(actionCreators.equivalentsLoad, dispatch),
    onTransferring: bindActionCreators(actionCreators.setTransferringMode, dispatch),
    onSend:  bindActionCreators(actionCreators.sendTokens, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BalancesContainer);
