import actionTypes from './actionTypes';
import { createAction } from 'redux-actions';
import { Contract, providers, utils } from 'ethers';
import abi from '../data/abi';
import tokens from '../data/tokens';
import {BigNumber, bigNumberify} from "ethers/utils";


let equivalentsLoad = (dispatch, token) => {
            dispatch(createAction(actionTypes.EQUIVALENT_LOAD_INIT)(token));
            let apiUrl = `https://min-api.cryptocompare.com/data/price?fsym=${token.symbol}&tsyms=USD`;
            fetch(apiUrl)
                .then(x => {
                    x.json().then(data => 
                    { 
                    dispatch(createAction(actionTypes.EQUIVALENT_LOAD_COMPLETE)({token: token, equivalent: data.USD}));
                });
                });
        };

const actionCreators = {

    setTransferringMode: createAction(actionTypes.TRANSFERRING_MODE_SET),

    loadAccounts: () => {
        return (dispatch) => {
            let currentProvider = window.web3.currentProvider;
            currentProvider.enable();
            let provider = new providers.Web3Provider(currentProvider);
            dispatch(createAction(actionTypes.ADDRESSES_LOAD_INIT)());
            provider.listAccounts()
                .then(data => {
                        dispatch(createAction(actionTypes.ADDRESSES_LOAD_COMPLETE)({addresses: data}));
                    }
                );
        }
    },

    changeAddress: (data) => {
        return (dispatch, getState) => {
            let currentProvider = window.web3.currentProvider;
            let provider = new providers.Web3Provider(currentProvider);
            dispatch(createAction(actionTypes.ADDRESS_CHANGED)(data));

            let address = getState().address.value;
            tokens.map(token => {
                dispatch(createAction(actionTypes.TOKENS_LOAD_INIT)(token));
                let contract = new Contract(token.address, abi, provider);
                contract.balanceOf(address).then(x => {
                    dispatch(createAction(actionTypes.TOKENS_LOAD_COMPLETE)({token: token, data: x}));
                    return equivalentsLoad(dispatch, token);
                });
            });
        }
    },

    sendTokens: (tokenAddress, sourceAddress, targetAddress, amount) => {
        return (dispatch, getState) => {

            let currentProvider = window.web3.currentProvider;
            let provider = new providers.Web3Provider(currentProvider);
            let signer = provider.getSigner(0);
                dispatch(createAction(actionTypes.SEND_TOKENS_INIT)(tokenAddress));
                let contract = new Contract(tokenAddress, abi, signer);
                let bigNumberAmount = bigNumberify((amount * Math.pow(10, 18)).toString()) ;//amount * Math.pow(10, 18); //bigNumberify(amount / Math.pow(10, 18));
                //contract.connect(signer);
                contract.transferFrom(sourceAddress, targetAddress, bigNumberAmount).then(_ => {
                    dispatch(createAction(actionTypes.TOKENS_LOAD_COMPLETE)());
                })
                    .catch(data => {
                    console.log(data);
            });
        }
    }
};

export default actionCreators;
