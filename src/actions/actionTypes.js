import keyMirror from 'keymirror';

const actions = keyMirror({
    ADDRESS_CHANGED: null,

    ADDRESSES_LOAD_INIT: null,
    ADDRESSES_LOAD_COMPLETE: null,

    TOKENS_LOAD_INIT: null,
    TOKENS_LOAD_COMPLETE: null,

    EQUIVALENT_LOAD_INIT: null,
    EQUIVALENT_LOAD_COMPLETE: null,

    TRANSFERRING_MODE_SET: null,

    SEND_TOKENS_INIT: null,
    SEND_TOKENS_COMPLETE: null,
});
export default actions;
